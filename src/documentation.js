// var Raise = require('raise');
// module.exports = Raise;

var org = `
* Here is a heading

And here _is_ some text under it.

** And here is a subheading :tagged:
:PROPERTIES:
:CREATED: [2016-08-11 Thu 08:48]
:END:

With a:

- bulleted
- list

* Table with heading
| here    | is   | a  | table |
| and     | here | is | the   |
| content | of   | it |       |

#+BEGIN_SRC html
<div class="here is some content"></div>
"
<
/
- another
- list
#+END_SRC

#+ARBITRARY_DIRECTIVE
<div class="whatever">yo</div>
#+END_DIRECTIVE
`;

var parser = require('org-mode-parser');
var string = parser.parseBigString(org);
// var body = parser.parseBigString(string[2].body);
// console.log(string);
console.log(new parser.OrgQuery(parser.parseBigString(string[2].body)).toHtml());


const hyper = require('hyperscript');
const hh = require('hyperscript-helpers')(hyper);
const { h1, div, p, span } = hh;

function render () {

    return hh.html({ lang: 'en-US' }, [
        hh.head(hh.script('var x = 5;')),
        hh.body(
            div('hi there'),
            div('hello'),
            div({
                style: 'background-color: yellow; width: 100px',
                class: 'test class',
                'data-attribute': true },
                'world'),
            div('another'))
    ]);
}

console.log(render().outerHTML);

/* The problem is that you're doing *two* template compilations. */
/* Is that a problem? No. Handlebars does the same. */
/* So there are ways to do this, but they require Babel. */
/* Do I care? */
/* A little... */
/* I would love something simpler. */
/* Well... strings are simpler. */
/* And it seems like your only real problem with them is syntax highlighting. */
/* Eh... jsx is good enough. Yes, it requires Babel. Yes, that's kind of a pain.
 * Ultimately, it's oK. */

/* Okay. `require.extension` is the 'proper' way to override this.
 * It comes with its own set of serious caveots, but for our purposes most
 * of them can be safely ignored. */

/* Using `require.extension` would also allow you to get around a lot of other crappy behavior. */
/* But you want to use it sparingly. */
/* I like the idea of having *one* require extension. */
/* So... Module._compile runs the code in the correct scope and then attaches it. */

/**
 // It would be /so easy/ to do sandboxing here.
 // You would need to worry about eval, I guess, but I suspect that's already handled.
 // You would need to worry about prototype polution, but again... that's not
 // *that* hard of a problem.
 // You would need to worry about global access.
 (function (exports, require, module, __filename, __dirname) {
   // module code goes here.
 }())
*/


/* So this is all nice. It would be very tempting to use `require` for everything.
 * It would be very tempting to try and get this to be a more minimal tool. */


/* What I want. I want to be able to define a map of files to structure very easily. */

/* pattern => { expand out into multiple files. } => select */


var posts = new Files('**/*.js');
/* get metadata from each file. */
//name, extension. --> that's easy. path.basename.


//so... what exactly is wrong with globby?
//needing to rewrite this every time is cumbersome. I want a way to handle the glue code.
//Nothing else... and highly extensible.

//A distilled.js but for directory transformations. Where I can just stick my common transformations into a less file.


var path = require('path');
var glob = require('fast-glob');
(async function () {
    var files = await glob('**/*.js');

    _.each(files, function (file) {
        var name = path.basename(file);
    });

    // Run pipe on each file.


    //iterative collection vs 
}());

(async () => {
    var fselector = function(){};
    var posts = await fselector('**/*.org'); // [file, file, file] -- extend from array?. I do like that.

    await posts.forEach(async (file) => {
        file.join('**/glob.js');

        // Does this mean everything will get grouped together?
        // I think so? It means you're adding to the array.

        //Alternatively, you could do something else...


        /* Parse org-data. */
        /* I'd like this to be something I can just call in a separate file. */
        /* This is where `process` would be nice. */

        //process data.
    });
})();
