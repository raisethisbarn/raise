/*
 * Non-relative paths are probably not essential,
 * but they give me a little bit of peace of mind.
 */
var path = require('path');
var root = (function () {
    return '';
}());

const TESTS = (function () {
    var glob = require('glob');

    return {
        units : glob.sync('**/*.unit.js', { cwd : root }),
        integrations : glob.sync('**/*.integration.js', { cwd : root })
    };
}());

async function units (suite) {
    var tests = TESTS.units;

    tests.forEach(function (testPath) {
        var test = require(path.resolve(root, testPath));
        var subSuite = suite.test(path.relative('src/node_modules', testPath));

        test(subSuite);
    });
}

//TODO: what is, strictly speaking, the difference between setup for a unit/integration test?
//I guess that integrations require a build to be run first.
async function integrations (suite) {
    var tests = TESTS.integrations;

    tests.forEach(function (testPath) {
        var test = require(path.resolve(root, testPath));
        var subSuite = suite.test(testPath);

        test(subSuite);
    });
}

/*
 * Actually running tests.
 * Distilled itself ends up composing most of the testing harness.
 * This just makes managing async code easier - there are fewer things to keep track of.
 */
var Distilled = require('@latinfor/distilled');
var suite = new Distilled();
suite.test('Units', units);
suite.test('Integrations', integrations);

/* Node still hasn't turned this behavior on by default */
process.on('unhandledRejection', function (up) { throw up; process.exit(1); });

